use cargo::CargoError;
use failure::Fail;
use reqwest::Error as ReqwestError;
use serde_json::Error as JsonError;

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "Cargo error")]
    Cargo(#[fail(cause)] CargoError),
    #[fail(display = "Dependency cycle detected: {:?}", _0)]
    Cycle(String),
    #[fail(display = "No Cargo.toml found")]
    NoCargoToml,
    #[fail(display = "Bad response from crates.io")]
    Registry(#[fail(cause)] RegistryError),
}

impl From<CargoError> for Error {
    fn from(e: CargoError) -> Self {
        Error::Cargo(e)
    }
}

impl From<RegistryError> for Error {
    fn from(e: RegistryError) -> Self {
        Error::Registry(e)
    }
}

#[derive(Debug, Fail)]
pub enum RegistryError {
    #[fail(display = "Could not parse response from crates.io")]
    Json(#[fail(cause)] JsonError),
    #[fail(display = "Request to crates.io failed")]
    Reqwest(#[fail(cause)] ReqwestError),
}

impl From<JsonError> for RegistryError {
    fn from(e: JsonError) -> Self {
        RegistryError::Json(e)
    }
}

impl From<ReqwestError> for RegistryError {
    fn from(e: ReqwestError) -> Self {
        RegistryError::Reqwest(e)
    }
}

pub type Result<T> = std::result::Result<T, Error>;
