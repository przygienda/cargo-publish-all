# cargo-publish-all

![coverage](https://gitlab.com/torkleyy/cargo-publish-all/badges/master/build.svg)
![license](https://img.shields.io/crates/l/cargo-publish-all/0.5.0.svg?style=popout-square)
![crate](https://img.shields.io/crates/v/cargo-publish-all.svg?style=popout-square)

**Use at your own risk.**

This tool scans the current workspace, builds every package as if it was published and publishes the ones that do
not exist yet on crates.io. If at any point the validation fails it will stop.

Validation consists of two phases:

1. Package & normalize all packages; replace version dependencies for crates that will be published with paths and try
   building every package.
2. If the first pass succeeds, publish all packages in the correct order. This will verify a second time, and should
   ideally never fail. However, it's better to be safe.

If an error is only detected in phase 2 (which should not happen), half of the workspace will be published (and build
successfully), the other half won't be on `crates.io`. This is definitely better than having broken packages on
crates.io.

## Usage

```sh
cargo install cargo-publish-all
cargo-publish-all
```

Note that you need to confirm the "publishing plan" by entering "y". If you don't want this, specify `--yes`:

```sh
cargo-publish-all --yes
```

---

Since this tool is still in its early phase, it may not always work as expected. Thus, you can use `--dry-run`
to make sure it behaves as expected, without pushing to crates.io:

```sh
cargo-publish-all --dry-run
```

**Note:** This will always fail for the second crate, which is a known issue. You can still see what it would do, though.
This should be improved in the future.

### Showing more output

By default, `cargo-publish-all` will hide all output except errors and high-level status messages since the output
will become quite big. You can change that using the `--verbose` flag.

### Docker

There is a docker image for using `cargo-publish-all`. It can be accessed using

```
registry.gitlab.com/torkleyy/cargo-publish-all
```

### GitLab CI

Note that you should have an environment variable `CRATES_IO_TOKEN` defined (and protected!).

```yaml
crates.io:
  image: registry.gitlab.com/torkleyy/cargo-publish-all:latest
  stage: deploy
  script:
  - cargo-publish-all --token $CRATES_IO_TOKEN --yes
  only: 
    refs:
    - master
```

**This will allow everybody with write access to trigger a push to crates.io.**
**Also note that secret varibles are not masked and can be revealed by merging a changed script.**

### Help

```
cargo-publish-all --help
cargo-publish-all 0.2
Thomas Schaller <torkleyy@gmail.com>
Upload workspace packages to crates.io

USAGE:
    cargo-publish-all [FLAGS] [OPTIONS]

FLAGS:
        --allow-dirty      Allow dirty working directories to be packaged
        --dry-run          Perform all checks without uploading
    -h, --help             Prints help information
        --manifest-path    Path to Cargo.toml
    -V, --version          Prints version information
        --verbose          Show more output
        --yes              Confirm publishing without interaction

OPTIONS:
        --token <TOKEN>    Token to use when uploading
```

### Example output

```
$ cargo-publish-all     
   Resolving workspace
    Fetching crates.io versions
Skipping crate `nitric-lock-internals`, version already published
Skipping crate `nitric`, version already published
   Verifying workspace
   Verifying nitric-lock-internals
   Verifying nitric-lock
   Verifying nitric
Summary:
nitric-lock-internals: 0.0.1 (skipped)
nitric-lock: 0.0.1
nitric: 0.0.1 (skipped)
Continue? [y/N] y
  Publishing workspace
    Skipping nitric-lock-internals
  Publishing nitric-lock
    Skipping nitric
```

## Behavior

The high-level behavior can be described as follows:

Publishes every unpublished crate of the current workspace, provided all members of the workspace
will build when pulled from crates.io.

This means you can simply run this on CI and it will publish crates as soon as the version number
is bumped.

### Example cases

> Note: the following properties should be tested in the future

* Executing `cargo-publish-all` a second time should have no effect
* `cargo-publish-all` for a single-crate workspace is equivalent to `cargo publish`

## TODO:

* Proper logging, no use of `println!`
* show how versions are bumped (`0.1.2` -> `0.1.3`)
    * bumping older versions (how to display this?)
* try merging this into Cargo
    * maybe too opinionated?
    * `cargo-publish-all` is idompotent, `cargo publish` not
